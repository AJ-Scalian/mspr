<?php
/**
 * Created by PhpStorm.
 * User: Utilisateur
 * Date: 31/10/2019
 * Time: 11:28
 */

include "Classes/Config/Config.php";
include "Classes/Config/ConfigEnv.php";
include "Classes/Config/ConnexionBdd.php";

$file = "base_joconde_court.csv";
$handler = fopen($file, "r");
$domaines = [];
$denomination = [];
$bdd = new \Classes\Config\ConnexionBdd();
$conn = $bdd->getBddConnexion();

$reqSelect = $conn->query("SELECT * FROM domaines;");
while ($domaine = $reqSelect->fetch()){
    $domaines[$domaine["id"]] = $domaine["nom"];
}

$i = 0;
while (($data = fgetcsv($handler, 0, ";")) !== FALSE){
    if ($i > 0) {
        $domainesCSV = explode(";", $data[1]);
        $denomination = $data[2];
        $titre = $data[4];
        $auteur = $data[5];
        $lieu_conservation = $data[27];
        $ville = $data[34];
        $geolocation = $data[35];
        $date_acquisition = $data[23];
        foreach ($domainesCSV as $domaine) {
            $domaine = ucfirst(strtolower($domaine));
            if (!in_array($domaine, $domaines)) {
                $reqId = $conn->query("SELECT MAX(id) FROM domaines");
                $reqId->execute();
                $idDomaine = $reqId->fetchColumn() + 1;
                $domaines[$idDomaine] = $domaine;
                $reqInsert = $conn->prepare("INSERT INTO domaines (id, nom) VALUES (:id,:nom)");
                $reqInsert->bindValue(":id", $idDomaine, PDO::PARAM_INT);
                $reqInsert->bindValue(":nom", $domaine, PDO::PARAM_STR);
                $reqInsert->execute();
            }
        }
    }
    $i++;
}
var_dump($domaines);

