<?php
/**
 * Created by PhpStorm.
 * User: Utilisateur
 * Date: 31/10/2019
 * Time: 11:28
 */

include "Classes/Config/Config.php";
include "Classes/Config/ConfigEnv.php";
include "Classes/Config/ConnexionBdd.php";

$file = "base_joconde_court.csv";
$handler = fopen($file, "r");
$domaines = [];
$denomination = [];
$bdd = new \Classes\Config\ConnexionBdd();
$conn = $bdd->getBddConnexion();

$reqSelect = $conn->query("SELECT * FROM domaines;");
while ($domaine = $reqSelect->fetch()){
    $domaines[$domaine["id"]] = $domaine["nom"];
}

$i = 0;
while (($data = fgetcsv($handler, 0, ";")) !== FALSE){
    if ($i > 0) {
        $reqIdOeuvre = $conn->query("SELECT MAX(id) FROM oeuvres");
        $reqIdOeuvre->execute();
        $idOeuvre = $reqIdOeuvre->fetchColumn() + 1;
        $domainesCSV = explode(";", $data[1]);
        $denominationsCSV = explode(";", $data[2]);
        $titre = ucfirst(strtolower($data[4]));
        $auteur = $data[5];
        $lieu_conservation = $data[27];
        $ville = $data[34];
        $geolocation = $data[35];
        $date_acquisition = $data[23];
        $tabLatLong = explode(",", $geolocation);
        $insertOeuvre = $conn->prepare("INSERT INTO oeuvres (id, titre, auteur, lieu_conservation, ville, latitude, longitude, date_acquisition)
          VALUES (:id, :titre, :auteur, :lieu_conservation, :ville, :lat, :long, :date_ac)");
        $insertOeuvre->bindValue(":id", $idOeuvre, PDO::PARAM_INT);
        $insertOeuvre->bindValue(":titre", $titre, PDO::PARAM_STR);
        $insertOeuvre->bindValue(":auteur", $auteur, PDO::PARAM_STR);
        $insertOeuvre->bindValue(":lieu_conservation", $lieu_conservation, PDO::PARAM_STR);
        $insertOeuvre->bindValue("ville", $ville, PDO::PARAM_STR);
        $insertOeuvre->bindValue("lat", $tabLatLong[0], PDO::PARAM_INT);
        $insertOeuvre->bindValue("long", $tabLatLong[1], PDO::PARAM_INT);
        $insertOeuvre->bindValue("date_ac", $date_acquisition, PDO::PARAM_STR);
        $insertOeuvre->execute();

        foreach ($domainesCSV as $domaine) {
            $domaine = ucfirst(strtolower($domaine));
            $reqIdDomaine = $conn->prepare("SELECT id FROM domaines WHERE nom = :nom");
            $reqIdDomaine->bindValue(":nom", $domaine, PDO::PARAM_STR);
            $reqIdDomaine->execute();
            $idDomaine = $reqIdDomaine->fetchColumn();
            var_dump($idDomaine);
            $reqInsert = $conn->prepare("INSERT INTO oeuvres_domaines (id_oeuvre, id_domaine) VALUES (:id_oeuvre, :id_domaine)");
            $reqInsert->bindValue(":id_oeuvre", $idOeuvre, PDO::PARAM_INT);
            $reqInsert->bindValue(":id_domaine", $idDomaine, PDO::PARAM_INT);
            $reqInsert->execute();
        }
        foreach ($denominationsCSV as $denom) {
            $denom = ucfirst(strtolower($denom));
            $reqIdDenom = $conn->prepare("SELECT id FROM denominations WHERE nom = :nom");
            $reqIdDenom->bindValue(":nom", $denom, PDO::PARAM_STR);
            $reqIdDenom->execute();
            $idDenom = $reqIdDenom->fetchColumn();
            var_dump($idDenom);
            $reqInsert = $conn->prepare("INSERT INTO oeuvres_denominations (id_oeuvre, id_denomination) VALUES (:id_oeuvre, :id_denom)");
            $reqInsert->bindValue(":id_oeuvre", $idOeuvre, PDO::PARAM_INT);
            $reqInsert->bindValue(":id_denom", $idDenom, PDO::PARAM_INT);
            $reqInsert->execute();
        }
    }
    $i++;
}


