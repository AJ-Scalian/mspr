<?php

use Classes\Config\Config;
use Classes\Config\ConfigEnv;

define('DS', DIRECTORY_SEPARATOR); // meilleur portabilité sur les différents systeme.
define('ROOT', dirname(__FILE__).DS); // pour se simplifier la vie


require_once "Classes/Config/Config.php";
require_once "Classes/Config/ConfigEnv.php";

$confEnv = Config::getConfigEnv();
require_once("Autoloader.php");
Autoloader::register();

?>
