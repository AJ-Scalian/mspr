<?php

namespace Classes\Config;

/**
 * Class ConnexionBdd : Classe qui gère la onnexion à une base de données via un objet PDO : c'est elle qui initialise l'objet et la connexion à la base
 * @package Classes\Config
 */
class ConnexionBdd {

    private $bddConnexion;

    /**
     * ConnexionBdd constructor : Crée une nouvelle instance d'objet PDO
     */
    public function __construct(){
        $config = Config::getConfigEnv();
        try
        {
            $this->bddConnexion = new \PDO('mysql:host='.$config->getHost().';dbname='.$config->getDbName().';charset=utf8', $config->getDbUser(), $config->getDbPassword());
        }
        catch (\Exception $e)
        {
            die('Erreur : ' . $e->getMessage());
        }
    }

    /**
     * @return \PDO
     */
    public function getBddConnexion()
    {
        return $this->bddConnexion;
    }

    /**
     * @param \PDO $bddConnexion
     */
    public function setBddConnexion($bddConnexion)
    {
        $this->bddConnexion = $bddConnexion;
    }

}

?>