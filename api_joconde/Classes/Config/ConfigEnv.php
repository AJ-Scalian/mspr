<?php
/**
 * Created by PhpStorm.
 * User: Utilisateur
 * Date: 26/08/2019
 * Time: 01:11
 */

namespace Classes\Config;

class ConfigEnv
{

    // Propriétés de la base : Config prod
    private $host;
    private $dbName;
    private $dbUser;
    private $dbPassword;
    private $racine;
    private $racineUrl;

    /**
     * ConfigBdd constructor.
     * @param string $host
     * @param string $dbName
     * @param string $dbUser
     * @param string $dbPassword
     * @param string $racine
     * @param string $racineUrl
     */
    public function __construct($host, $dbName, $dbUser, $dbPassword, $racine, $racineUrl)
    {
        $this->host = $host;
        $this->dbName = $dbName;
        $this->dbUser = $dbUser;
        $this->dbPassword = $dbPassword;
        $this->racine = $racine;
        $this->racineUrl = $racineUrl;
    }


    /**
     * @return string
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @return string
     */
    public function getDbName()
    {
        return $this->dbName;
    }

    /**
     * @return string
     */
    public function getDbUser()
    {
        return $this->dbUser;
    }

    /**
     * @return string
     */
    public function getDbPassword()
    {
        return $this->dbPassword;
    }

    /**
     * @return string
     */
    public function getRacine()
    {
        return $this->racine;
    }

    /**
     * @return mixed
     */
    public function getRacineUrl()
    {
        return $this->racineUrl;
    }

    /**
     * @param string $host
     */
    public function setHost($host)
    {
        $this->host = $host;
    }

    /**
     * @param string $dbName
     */
    public function setDbName($dbName)
    {
        $this->dbName = $dbName;
    }

    /**
     * @param string $dbUser
     */
    public function setDbUser($dbUser)
    {
        $this->dbUser = $dbUser;
    }

    /**
     * @param string $dbPassword
     */
    public function setDbPassword($dbPassword)
    {
        $this->dbPassword = $dbPassword;
    }

    /**
     * @param string $racine
     */
    public function setRacine($racine)
    {
        $this->racine = $racine;
    }

    /**
     * @param mixed $racineUrl
     */
    public function setRacineUrl($racineUrl)
    {
        $this->racineUrl = $racineUrl;
    }



}