<?php 

namespace Classes\Config;

/**
 * Class Config : Gère la configuration générale du backoffice grâce à des propriétés statiques
 * @package Classes\Config
 */
class Config {

    // Propriétés de l'application (liens internes, externes, chamins, variables)
    
    public static function getConfigEnv(){
        return new ConfigEnv("localhost","id3126091_snapat", "id3126091_snapat", "lololepro24", "/public_html/api_joconde/", "/api_joconde/");
        //return new ConfigEnv("localhost", "joconde", "root", "", "C:\\\\wamp64\\www\\api_joconde\\", "/api_joconde/");
    }

}
