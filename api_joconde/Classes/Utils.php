<?php

namespace Classes;

/**
 * Class Utils : Classe utilitaire composée de méthodes statiques globales à l'application qui effectuent des traitements redondants et utilisables n'importe ou dans le code
 * @package Classes\Config
 */
class Utils {

    /**
     * Méthode qui upload un fichier d'un formulaire sur le serveur
     * @param string $index : l'index du fichier dans le formulaire
     * @param string $nom_fichier : le nouveau nom du fichier sur le serveur
     * @param string $rep : le répertoire ou le fichier doit être placé (par défaut : documents)
     * @param array $extensions_autorisees : Le tableau des extensions de fichiers autorisés
     * @return bool : true si le fichier à bien été importé, false s'il n'a pas été uploadé pour X raison
     */
    public static function upload($index, $nom_fichier, $rep = '/var/www/pt/documents/', $extensions_autorisees = array('jpg', 'JPG', 'jpeg', 'JPEG', 'gif', 'GIF', 'png', 'PNG')){
        if (isset($_FILES[$index]))
        {
            if ($_FILES[$index]['error'] == 0)
            {
                // Testons si le fichier n'est pas trop gros
                if ($_FILES[$index]['size'] <= 100000000)
                {
                    // Testons si l'extension est autorisée
                    $infosfichier = pathinfo($_FILES[$index]['name']);
                    $extension_upload = $infosfichier['extension'];
                    if (in_array($extension_upload, $extensions_autorisees))
                    {
                        // On peut valider le fichier et le stocker définitivement
                        return move_uploaded_file($_FILES[$index]['tmp_name'], $rep . $nom_fichier);
                    }
                    return false;
                }
                return false;
            }
            return false;
        }
        return true;
    }

    /**
     * Assainis un tableau de variable (par exemple $_POST pour un form : empêche la saisie d'espaces avant et après et l'injection de code malveillant coté client
     * @param array $array Le tableau de données à assainir
     * @return array Le tableau de données assainies
     */
    public static function sanitize(array $array){
        $arraySanitized = [];
        foreach($array as $key => $val){
            $arraySanitized[$key] = htmlspecialchars(trim($val));
        }
        return $arraySanitized;
    }

    /**
     * Verifie qu'un fichier existe sur l'URL distante passé en paramètre
     * @param string $url : URL dont l'existence doit etre vérifiée
     * @return bool : true si l'url existe, false sinon
     */
    public static function url_exists($url)
    {
        $handle = @fopen($url, "r");
        if ($handle === false)
            return false;
        fclose($handle);
        return true;
    }

}

?>