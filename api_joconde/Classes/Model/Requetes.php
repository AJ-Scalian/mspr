<?php

namespace Classes\Model;

use \Classes\Config\Config;

class Requetes {

    private $url;

    public function __construct($url){
        $this->url = Config::$URL_API_BASE . $url;
    }

    public function sendPostRequest($data){                                                                
        $data_string = json_encode($data);                                                                                                  
        $ch = curl_init('http://api.local/rest/users');                                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data_string))                                                                       
        );                                                                                                                 
        return $result = curl_exec($ch);
    }

}

?>