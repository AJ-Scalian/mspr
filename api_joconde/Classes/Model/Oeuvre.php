<?php
/**
 * Created by PhpStorm.
 * User: Utilisateur
 * Date: 30/10/2019
 * Time: 10:14
 */

namespace Classes\Model;


class Oeuvre extends BDDHydrate
{

    public $id;
    public $titre;
    public $auteur;
    public $lieu_conservation;
    public $ville;
    public $latitude;
    public $longitude;
    public $date_acquisition;

    /**
     * @return mixed
     */
    public function get_id()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function set_id($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function get_titre()
    {
        return $this->titre;
    }

    /**
     * @param mixed $titre
     */
    public function set_titre($titre)
    {
        $this->titre = $titre;
    }

    /**
     * @return mixed
     */
    public function get_auteur()
    {
        return $this->auteur;
    }

    /**
     * @param mixed $auteur
     */
    public function set_auteur($auteur)
    {
        $this->auteur = $auteur;
    }

    /**
     * @return mixed
     */
    public function get_lieu_conservation()
    {
        return $this->lieu_conservation;
    }

    /**
     * @param mixed $lieu_conservation
     */
    public function set_lieu_conservation($lieu_conservation)
    {
        $this->lieu_conservation = $lieu_conservation;
    }

    /**
     * @return mixed
     */
    public function get_ville()
    {
        return $this->ville;
    }

    /**
     * @param mixed $ville
     */
    public function set_ville($ville)
    {
        $this->ville = $ville;
    }

    /**
     * @return mixed
     */
    public function get_latitude()
    {
        return $this->latitude;
    }

    /**
     * @param mixed $latitude
     */
    public function set_latitude($latitude)
    {
        $this->latitude = $latitude;
    }

    /**
     * @return mixed
     */
    public function get_longitude()
    {
        return $this->longitude;
    }

    /**
     * @param mixed $longitude
     */
    public function set_longitude($longitude)
    {
        $this->longitude = $longitude;
    }

    /**
     * @return mixed
     */
    public function get_date_acquisition()
    {
        return $this->date_acquisition;
    }

    /**
     * @param mixed $date_acquisition
     */
    public function set_date_acquisition($date_acquisition)
    {
        $this->date_acquisition = $date_acquisition;
    }




}