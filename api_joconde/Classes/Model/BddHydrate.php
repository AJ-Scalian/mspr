<?php

namespace Classes\Model;

/**
 * Class BDDHydrate Classe abstraite implémentant l'interface Hydratable, parente de tous les objets pouvant être hydratés depuis une BDD
 * @package Classes\Model
 */
abstract class BDDHydrate implements Hydratable {

    /**
     * Implémentation de la méthode hydrater pour les classes pouvan etre hydratées depuis des données issues d'une BDD
     * @param array $donnees les données issues de la base
     */
    public function hydrater(array $donnees)
    {
      foreach ($donnees as $key => $value)
      {
        // On récupère le nom du setter correspondant à l'attribut.
        $method = 'set_'.$key;

        // Si le setter correspondant existe.
        if (method_exists($this, $method))
        {
          // On appelle le setter.
          $this->$method($value);
        }
      }
    }

}

?>