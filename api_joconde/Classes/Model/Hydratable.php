<?php 

namespace Classes\Model;

/**
 * Interface Hydratable : Interface qui implémentée désigne un objet "Hydratable", qu'on peut remplir de données.
 * @package Classes\Model
 */
interface Hydratable {

    /**
     * Fonction qui set les données aux classes l'implémentant
     * @param array $data Les données à fournir à la classe
     * @return mixed
     */
    public function hydrater(array $data);

}

?>