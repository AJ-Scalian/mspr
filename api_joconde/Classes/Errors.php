<?php

namespace Classes;

use Classes\Config\Config;

class Errors {

    private $url_redirect;

    public function __construct($url_redirect){
        $env = Config::getConfigEnv();
        $this->url_redirect = $env->getRacineUrl() . $url_redirect;
    }

    private static $errors = array(
        404 => "Oeuvre non trouvée"
    );

    public static function writeError($error){
        echo htmlspecialchars(trim(Errors::$errors[$error]));
    }

    public function setError($error){
        $sep = "?";
        if (stristr($this->url_redirect, "?") !== false)
            $sep = "&";
        header("Location: " . $this->url_redirect . $sep . "error=" . $error);
        //die;
    }

}

?>