<?php
/**
 * Created by PhpStorm.
 * User: Utilisateur
 * Date: 30/07/2019
 * Time: 00:08
 */

namespace Classes\Controller;

use \Classes\Config\Config;
use \Classes\Utils;

/**
 * Class Controller : Classe abstraite à implémenter pour gérer les requêtes HTTP, prendre si besoin des modèles et charger des vues
 * @package Classes\Controller
 */
abstract class Controller
{

    private $url;
    private $userConnecte;
    private $requestGet;
    private $requestPost;

    /**
     * Controller constructor : Nouvelle instance de controller, récupère les contenus des requêtes et les assainis et affecte l'utilisateur connecté
     */
    public function __construct()
    {
        $this->requestGet = Utils::sanitize($_GET);
        $this->requestPost = Utils::sanitize($_POST);
    }


    /**
     * Méthode à implémenter pour gérer la requête HTTP et en déduire la méthode à executer
     * @return mixed
     */
    public abstract function handleRequest();

    /**
     * Récupère l'action à effectuer passée dans l'URL
     * @return null|string
     */
    public function getAction(){
        return $action = isset($_GET["action"]) ? $_GET["action"] : null;
    }

    /**
     * Récupère l'Id passé en paramètre URL s'il y en a un
     * @return null|string null si la variable id n'est pas passée en paramètre d'URL, la valeur sinon
     */
    public function getId(){
        return $id = isset($_GET["id"]) ? htmlspecialchars(trim($_GET["id"])) : null;
    }

    public function getUserConnecte(){
        return $this->userConnecte;
    }

    public function getRequestGet(){
        return $this->requestGet;
    }
    public function getRequestPost(){
        return $this->requestPost;
    }

    public function getUrl(){
        return $this->url;
    }
    public function setUrl($url){
        $this->url = $url;
    }

}