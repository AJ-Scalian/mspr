<?php
/**
 * Created by PhpStorm.
 * User: Utilisateur
 * Date: 30/10/2019
 * Time: 10:10
 */

namespace Classes\Controller;


use Classes\DAO\OeuvreDAO;
use Classes\Errors;
use Classes\Model\Oeuvre;

class OeuvreController extends Controller
{

    private $dao;

    /**
     * EntrepriseController constructor : Affecte l'URL, crée une instance d'EntreprisesDAO et d'Errors pour la gestion des entreprises.
     * Si l'utilisateur n'est pas connecté, empeche l'accès a ces fonctions et redirige vers la connexion
     */
    public function __construct(){
        parent::__construct();
        $this->setUrl("oeuvres.php");
        $this->dao = new OeuvreDAO();
    }

    /**
     * Implémentation de la méthode pour gérer l'action à effectuer et executer la bonne méthode
     * @return mixed|void
     */
    public function handleRequest(){
        $action = $this->getAction();
        switch ($action) {
            case "get-all": $this->getAllOeuvresAction(); break;
        }
    }

    public function getAllOeuvresAction(){
        $oeuvres = $this->dao->getAll();
        echo json_encode($oeuvres);
    }

}