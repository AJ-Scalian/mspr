<?php
/**
 * Created by PhpStorm.
 * User: Utilisateur
 * Date: 30/10/2019
 * Time: 10:12
 */

namespace Classes\DAO;


class OeuvreDAO extends DAO
{

    public function __construct()
    {
        parent::__construct("oeuvres", "id", "Oeuvre");
    }

}