<?php

namespace Classes\DAO;

use Classes\Config\ConnexionBdd;

class DAO {

    private $table;
    private $primaryKey;
    private $class;
    protected $bdd;

    public function exists($champ, $val){
        $table = $this->getTable();
        $req = $this->bdd->prepare("SELECT $champ FROM $table WHERE $champ = :val");
        $req->bindValue(":val", $val, \PDO::PARAM_STR);
        $req->execute();
        if ($req->rowCount() > 0)
            return true;
        return false;
    }

    public function __construct($table, $primaryKey, $class){
        $this->table = $table;
        $this->primaryKey = $primaryKey;
        $this->class = "Classes\\Model\\".$class;
        $this->bdd = (new ConnexionBdd())->getBddConnexion();
    }

    public function dataArrayToObj(array $dataArray){
        $class = $this->class;
        $newObj = new $class();
        $newObj->hydrater($dataArray);
        return $newObj;
    }
    
    // Transforme une 
    public function listDataArrayToObj($listDataArray){
        $arrayObj = [];
        foreach ($listDataArray as $arrayVal){
            $arrayObj[] = $this->dataArrayToObj($arrayVal);
        }
        return $arrayObj;
    }

    public function getById($id){
        $req = $this->bdd->prepare("SELECT * FROM $this->table WHERE $this->primaryKey = :id AND is_active = 1");
        $req->bindValue(":id", $id, \PDO::PARAM_INT);
        $req->execute();
        if ($req->rowCount() < 1)
            return false;
        return $this->dataArrayToObj($req->fetch());
    }

    public function search($arrayChamps, $searchVal){
        $sql = "SELECT * FROM $this->table WHERE is_active = 1 AND (";
        foreach ($arrayChamps as $k => $champ) {
            if ($k >= 1)
                $sql .= " OR ";
            $sql .= " $champ LIKE :search";
        }
        $req = $this->bdd->prepare($sql . ")");
        $req->bindValue(":search", "%".$searchVal."%");
        $req->execute();
        return $this->listDataArrayToObj($req->fetchAll());
    }

    public function supprimer($id){
        $req = $this->bdd->prepare("UPDATE $this->table SET is_active = 0 WHERE $this->primaryKey = :id");
        $req->bindValue(":id", $id, \PDO::PARAM_INT);
        return $req->execute();
    }

    protected function getOrderByStr(array $orderBy = array()){
        $orderByStr = count($orderBy) > 0 ? " ORDER BY " : "";
        $i = 0;
        foreach ($orderBy as $k => $order){
            if ($i > 0 && $i < count($orderBy))
                $orderByStr .= ", ";
            $orderByStr .= "$k $order";
            $i++;
        }
        return $orderByStr;
    }

    public function getBy($champ, $val, $PARAM = \PDO::PARAM_STR, array $orderBy = array()){
        $orderByStr = $this->getOrderByStr($orderBy);
        $req = $this->bdd->prepare("SELECT * FROM $this->table WHERE $champ = :val AND is_active = 1 $orderByStr");
        $req->bindValue(":val", $val, $PARAM);
        $req->execute();
        return $this->listDataArrayToObj($req->fetchAll());
    }

    public function getAll($orderBy = array()){
        $orderByStr = $this->getOrderByStr($orderBy);
        $req = $this->bdd->query("SELECT * FROM $this->table ");
        if ($req->execute())
            return $this->listDataArrayToObj($req->fetchAll());      
        return false;
    }

    public function chargerImg($nomChamp, $nomFichier, $id){
        $req = $this->bdd->prepare("UPDATE $this->table SET $nomChamp = :nomFichier WHERE $this->primaryKey = :id");
        $req->bindValue(":nomFichier", $nomFichier, \PDO::PARAM_STR);
        $req->bindValue(":id", $id, \PDO::PARAM_INT);
        return $req->execute();
    }

    public function getTable(){
        return $this->table;
    }
    public function getClass(){
        return $this->class;
    }
    public function getPrimaryKey(){
        return $this->primaryKey;
    }

}

?>