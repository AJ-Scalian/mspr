import { Component, OnInit, Input } from '@angular/core';
import { DataRetrieveService } from '../_services/data-retrieve.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  data: any;
  @Input() searchText: string;
  @Input() search: string;


  constructor(public dataretrieveService: DataRetrieveService) { 
     
  }

  ngOnInit() {
    this.data = this.dataretrieveService.send_data();
    /*this.dataretrieveService.send_data()
        .subscribe(heroes => this.data = heroes);*/
  }

  getData() {
    
     // this.data = this.dataretrieveService.send_data();
  }

}
