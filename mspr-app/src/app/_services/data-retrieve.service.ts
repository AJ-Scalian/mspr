import { Injectable } from '@angular/core';






@Injectable({
  providedIn: 'root'
})



export class DataRetrieveService {
  public data: any = [];


  
  constructor() { 
    this.data = [
      {
        id: 1,
        title: 'bonjour',
        text:"blabla"
      },
      {
        id: 2,
        title: 'bonjour',
        text:"blabla"
      },
      {
        id: 3,
        title: 'bonjour',
        text:"blabla"
      },
      {
        id: 4,
        title: 'bonjour',
        text:"blabla"
      },
    ]
  }

  send_data() {
    return this.data;
  }

  filterItems(searchTerm) {
    return this.data.filter(item => {
      return item.title.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
    });
  }
}
